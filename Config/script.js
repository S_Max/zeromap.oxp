/* global system log worldScripts player mission missionVariables */

this.name = "ZeroMap";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Hide map. Show only visited planets and nearby planets. Visited - all info. In 7LY - only names. In 10LY - as dots (for search isolated regions).";
this.version = "0.6";

"use strict";

this._DEBUG = false;

this._visitedSystems = [
    [],
    [],
    [],
    [],
    [],
    [],
    [],
    []
];

// 0 - full info
// 100 - name
// 200 - as dot
// 300 - hide
// [not vis, visited, 7LY, targets, 10LY]
this._LEVEL = [
    [200, 0, 0, 100, 100], // Easy
    [300, 0, 100, 200, 200], // Normal
    [300, 0, 200, 200, 300], // Hard
    [0, 0, 0, 0, 0] // Disable
];
this._LEVEL_CURRENT = 1;

this._timer_start = new Date();
this._logger = function(msg, ignore_ts) {
    if (this._DEBUG) {
        if (ignore_ts) {
            log(this.name, msg);
        }
        else {
            var t = new Date();
            log(this.name, msg + " [+" + (t.getTime() - this._timer_start.getTime()) + "ms]");
            this._timer_start = t;
        }
    }
};

// Импорт данных из Explorers Club
this._importFromExplorersClub = function() {
    this._logger("this._importFromExplorersClub()");

    var g = system.info.galaxyID;

    if (this._visitedSystems[g].length == 0) {

        this._visitedSystems[g] = [];
        for (var i = 0; i < 256; i++) {
            this._visitedSystems[g][i] = 0;
        }

        var EC = worldScripts["Explorers Club"];
        if (EC) {
            for (var i = 0; i < 256; i++) {
                if (EC._playerVisited(g, i)) {
                    this._markVisitedSystem(g, i);
                }
            }
        }
    }

    this._logger("this._importFromExplorersClub END");
};

// Показать все системы (чтобы отработал System.infoForSystem как надо)
this._showAll = function(g) {
    this._logger("this._showAll(" + g + ")");

    for (var i = 0; i < 256; i++) {
        System.infoForSystem(g, i).setProperty(2, "concealment", null);
    }

    this._logger("this._showAll END");
};

// Скрыть все системы согласно this._visitedSystems
this._hideAll = function(g) {
    this._logger("this._hideAll(" + g + ")");
    var settings = this._LEVEL[this._LEVEL_CURRENT];

    for (var i = 0; i < 256; i++) {
        var c = settings[this._visitedSystems[g][i]];
        if (typeof c == 'undefined') {
            c = 0;
        }
        System.infoForSystem(g, i).setProperty(2, "concealment", c);
    }

    this._logger("this._hideAll END");
};

// Пометить систему и ее соседей как видимые (полостью или частично)
this._markVisitedSystem = function(g, i) {
    this._logger("this._markVisitedSystem(" + g + "," + i + ")", 1);

    this._visitedSystems[g][i] = 1;
    var s = System.infoForSystem(g, i).systemsInRange();

    this._logger("Step 1: System.infoForSystem: " + s.length, 1);

    for (var j = 0; j < s.length; j++) {
        this._logger(s[j].name + ":" + this._visitedSystems[g][s[j].systemID]);
        if (this._visitedSystems[g][s[j].systemID] != 1) {
            this._visitedSystems[g][s[j].systemID] = 2;
        }
    }

    // Покажем ближайшие системы как результат визуального наблюдения
    s = System.infoForSystem(g, i).systemsInRange(10);

    this._logger("Step 2: System.infoForSystem: " + s.length, 1);

    for (var j = 0; j < s.length; j++) {
        this._logger(s[j].name + ":" + this._visitedSystems[g][s[j].systemID]);
        if (this._visitedSystems[g][s[j].systemID] == 0 || this._visitedSystems[g][s[j].systemID] == 3) {
            this._visitedSystems[g][s[j].systemID] = 4;
        }
    }

    this._logger("this._markVisitedSystem END", 1);
};

this._isInteger = function(value) {
    return typeof value === "number" &&
        isFinite(value) &&
        Math.floor(value) === value;
};

this._markTargetSystems = function() {
    this._logger("this._markTargetSystems()");

    var g = system.info.galaxyID;

    // Убирем старые отметки
    for (var i = 0; i < 256; i++) {
        if (this._visitedSystems[g][i] == 3) {
            this._visitedSystems[g][i] = 0;
        }
    }

    // Контракты на доставу
    var contracts = player.ship.contracts;
    for (var i = 0; i < contracts.length; i++) {
        this._markTarget(g, contracts[i].destination);
    }

    // Перевозка пассажиров
    var passengers = player.ship.passengers;
    for (var i = 0; i < passengers.length; i++) {
        this._markTarget(g, passengers[i].destination);
    }

    // Доставка посылок
    var parcels = player.ship.parcels;
    for (var i = 0; i < parcels.length; i++) {
        this._markTarget(g, parcels[i].destination);
    }

    // Отмеченные системы
    var markedSystems = mission.markedSystems;
    for (var i = 0; i < markedSystems.length; i++) {
        var obj = markedSystems[i];
        if (typeof obj.system != 'undefined') {
            if (obj.name != "exclubmap") { // Игнорим метки от Explorers Club (чтобы всю карту не показывать)
                this._markTarget(g, obj.system);
            }
        }
        else {
            this._markTarget(g, obj);
        }
    }

    this._logger("this._markTargetSystems() END");
};

this._markTarget = function(g, i) {
    this._logger("this._markTarget(" + g + "," + i + ")", 1);

    if (this._visitedSystems[g][i] == 0) {
        this._visitedSystems[g][i] = 3;
    }
};

// Выполнить "полный" цикл работы - импорт из ExplorersClub + малый цикл.
this._doWorkFull = function() {
    this._logger("this._doWorkFull()");

    this._importFromExplorersClub();
    this._doWork();

    this._logger("this._doWorkFull END");
};

// Выполнить "малый" цикл работы
this._doWork = function() {
    this._logger("this._doWork()");

    // Если мы не в межсистемном пространстве
    if (system.ID != -1) {
        var g = system.info.galaxyID;
        this._markVisitedSystem(g, system.ID);
    }

    this._logger("this._doWork END");
};

// Старт игры
this.startUpComplete = function() {
    this._logger("this.startUpComplete()");

    this._loadData();

    // Тестирование видимости и достижимости систем (проверка изолированных систем)
    //this._setInRangeTester();

    this._doWorkFull();

    this._showMission(player.ship.dockedStation);

    this._logger("this.startUpComplete END");
};

// Определение "Достижимых" и "видимых визуально" систем из текущей
this._setInRangeTester = function() {
    var arr = [];
    arr.push(system.ID);
    var g = system.info.galaxyID;
    this._visitedSystems = [
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        []
    ];
    for (var i = 0; i < 256; i++) {
        this._visitedSystems[g][i] = 0;
    }
    while (arr.length != 0) {
        this._logger(arr);
        var id = arr.pop();


        this._visitedSystems[g][id] = 1;
        var s = System.infoForSystem(g, id).systemsInRange();

        //this._logger("Step 1: System.infoForSystem: " + s.length, 1);

        for (var j = 0; j < s.length; j++) {
            //this._logger(s[j].name + ":" + this._visitedSystems[g][s[j].systemID]);
            if (this._visitedSystems[g][s[j].systemID] != 1) {
                this._visitedSystems[g][s[j].systemID] = 1;
                arr.push(s[j].systemID);
            }
        }

        // Покажем ближайшие системы как результат визуального наблюдения
        s = System.infoForSystem(g, id).systemsInRange(10);

        //this._logger("Step 2: System.infoForSystem: " + s.length, 1);

        for (var j = 0; j < s.length; j++) {
            //this._logger(s[j].name + ":" + this._visitedSystems[g][s[j].systemID]);
            if (this._visitedSystems[g][s[j].systemID] == 0) {
                this._visitedSystems[g][s[j].systemID] = 4;
            }
        }
    }

    this._logger("Not showing " + g);
    for (var i = 0; i < 256; i++) {
        if (this._visitedSystems[g][i] != 1) {
            var t = System.infoForSystem(g, id);
            this._logger(t.name + ":" + this._visitedSystems[g][i]);
        }
    }
    this._logger("Not showing END");
};

// Сохранение
this.playerWillSaveGame = function() {
    this._logger("this.playerWillSaveGame()");

    this._saveData();

    this._logger("this.playerWillSaveGame END");
};

// Выход из гипера
this.shipExitedWitchspace = function() {
    this._logger("this.shipExitedWitchspace()");

    this._doWork();

    this._logger("this.shipExitedWitchspace END");
};

// Переход между галактиками
this.playerEnteredNewGalaxy = function() {
    this._logger("this.playerEnteredNewGalaxy()");

    if (system.isInterstellarSpace) {
        return;
    }

    this._doWorkFull();

    this._logger("this.playerEnteredNewGalaxy END");
};

// Изменение экрана
this.guiScreenWillChange = function(to, from) {
    this._logger("this.guiScreenWillChange(): " + from + " -> " + to);

    this._mapShowOrHide(to, from);

    this._logger("this.guiScreenWillChange END");
};
this.guiScreenChanged = function(to, from) {
    this._logger("this.guiScreenChanged(): " + from + " -> " + to);

    this._mapShowOrHide(to, from);

    this._logger("this.guiScreenChanged END");
};

this._mapShowOrHide = function(to, from) {
    var g = system.info.galaxyID;

    if (to == 'GUI_SCREEN_SHORT_RANGE_CHART' || to == 'GUI_SCREEN_LONG_RANGE_CHART' || to == 'GUI_SCREEN_MISSION' || to == 'GUI_SCREEN_SYSTEM_DATA') {
        this._hideGUIScreen();
        //this._timer = new Timer(this, this._hideGUIScreen, 0, -1);
    }
    else {
        this._showAll(g);
    }
};

this._hideGUIScreen = function() {
    this._logger("this._hideGUIScreen()");

    var g = system.info.galaxyID;
    this._markTargetSystems();
    this._hideAll(g);

    this._logger("this._hideGUIScreen END");
};

this._saveData = function() {
    // Сохраняем в save-файл посещения
    missionVariables.ZeroMap_VISITED_SYSTEMS__0_3 = JSON.stringify(this._visitedSystems);
    missionVariables.ZeroMap_SETTINGS_LEVEL = this._DIFFICULTY[this._LEVEL_CURRENT];
};

this._loadData = function() {
    // Инициализируем посещения
    this._visitedSystems = [
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        []
    ];

    // Загружаем настройки
    var level = missionVariables.ZeroMap_SETTINGS_LEVEL;
    if (this._DIFFICULTY.indexOf(level) != -1) {
        this._logger("Level: " + level);
        this._LEVEL_CURRENT = this._DIFFICULTY.indexOf(level);
    }

    // Загружаем посещения
    var tmp = JSON.parse(missionVariables.ZeroMap_VISITED_SYSTEMS__0_3);

    // Если загрузка не произошла - пытаемся импортировать исторические данные
    if (!tmp) {
        tmp = this._importFromOld();
    }

    // Если загрузили успешно
    if (tmp) {
        this._visitedSystems = tmp;
    }
};

this._importFromOld = function() {
    // Удаляем данные до версии 0.3
    delete missionVariables.ZeroMap_VISITED_SYSTEMS;

    return null;
};

this.shipDockedWithStation = function(station) {
    this._showMission(station);
};

this._DIFFICULTY = ["Easy", "Normal", "Hard", "Disable"];

this._showMission = function(station) {
    this._logger("ZeroMap difficulty: " + this._LEVEL_CURRENT + " : " + this._DIFFICULTY[this._LEVEL_CURRENT]);

    station.setInterface("ZeroMap_OXP", {
        title: "ZeroMap",
        category: "Options",
        summary: "ZeroMap OXP settings",
        callback: this._missionCallBack.bind(this)
    });
};

this._missionCallBack = function(state) {

    player.ship.hudHidden = false;
    var states = ["ZeroMap_01_EASY", "ZeroMap_02_NORMAL", "ZeroMap_03_HARD", "ZeroMap_04_OFF"];

    if (state == "ZeroMap_OXP") {

        var choices_arr = [{
            text: "Easy",
            //alignment: "LEFT"
        }, {
            text: "Normal",
            //alignment: "LEFT"
        }, {
            text: "Hard",
            //alignment: "LEFT"
        }, {
            text: "Disable",
            //alignment: "LEFT"
        }];

        var choices = {};
        for (var i = 0; i < choices_arr.length; i++) {
            choices[states[i]] = choices_arr[i];
        }

        choices.ZeroMap_00_Diff = {
            text: "Difficulty",
            //alignment: "LEFT",
            unselectable: true,
            color: "whiteColor"
        };

        choices.ZeroMap_50 = {
            text: "",
            //alignment: "LEFT"
        };

        choices.ZeroMap_99_EXIT = {
            text: "Exit",
            //alignment: "LEFT"
        };

        var current = current = choices_arr[this._LEVEL_CURRENT];
        current.text = "* " + current.text + " *";
        current.color = "greenColor";

        player.ship.hudHidden = true;
        mission.runScreen({
                title: "Select ZeroMap difficulty",
                message: this._buildDescription(this._difficultyDescription),
                choices: choices,
                initialChoicesKey: "ZeroMap_99_EXIT",
                exitScreen: "GUI_SCREEN_SHORT_RANGE_CHART"
            },
            this._missionCallBack);
    }

    if (states.indexOf(state) != -1) {
        this._LEVEL_CURRENT = states.indexOf(state);

        player.ship.hudHidden = true;
        mission.runScreen({
                allowInterrupt: true,
                title: "ZeroMap difficulty: " + this._DIFFICULTY[this._LEVEL_CURRENT],
                message: "Saved. Current settings are:\n\n" + this._buildDescription(this._difficultyDescription[this._LEVEL_CURRENT]),
                //backgroundSpecial: "SHORT_RANGE_CHART",
                exitScreen: "GUI_SCREEN_SHORT_RANGE_CHART"
            },
            this._missionCallBack);
    }
};

this._buildDescription = function(desc) {
    var str = "";

    for (var i = 0; i < desc.length; i++) {
        var d = desc[i];
        if (Array.isArray(d)) {
            str += this._buildDescription(d) + "\n";
        }
        else {
            str += d + "\n";
        }
    }

    return str;
};

this._difficultyDescription = [
    [
        "Easy - shows full map of the Galaxy without names of systems.",
        "* Visited systems - all information.",
        "* Systems in 7LY - all information.",
        "* Systems in 10LY - only name of system."
    ],
    [
        "Normal - hides map of the Galaxy. Some information about the surrounding systems are available. Isolated regions can be founded.",
        "* Visited systems - all information.",
        "* Systems in 7LY - only name of system.",
        "* Systems in 10LY - without name of system."
    ],
    [
        "Hard - hides map of the Galaxy. Minimum information about the surrounding systems are available.",
        "* Visited systems - all information.",
        "* Systems in 7LY - without name of system."
    ],
    [
        "Disable - shows standard map of the Galaxy."
    ]
];