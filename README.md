Zero Map

By S Max

Hide map. Show only visited planets and nearby planets.

Difficulty levels:
------------------

 * Easy - shows full map of the Galaxy without names of systems.
     * Visited systems - all information.
     * Systems in 7LY - all information.
     * Systems in 10LY - only name of system.
 * Normal - hides map of the Galaxy. Some information about the surrounding systems are available. Isolated regions can be founded.
     * Visited systems - all information.
     * Systems in 7LY - only name of system.
     * Systems in 10LY - without name of system.
 * Hard - hides map of the Galaxy. Minimum information about the surrounding systems are available.
     * Visited systems - all information.
     * Systems in 7LY - without name of system.
 * Disable - shows standard map of the Galaxy.

Available in [Oolite](http://www.oolite.org/) package manager (Mechanics).

License
=======
This work is licensed under the Creative Commons Attribution-Noncommercial-Share Alike 4.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/

Version History
===============

Release 0.6

* Fix additional store concealment state in save-file

Release 0.5

* Add difficulty levels.

Release 0.4

* In 1.84 work fine
* Visual observation allows to define system 10LY (for searching isolated systems).

Release 0.3

* Show marked systems or target systems in missions
* Map hudes only on map screens (and system info). Mission generation work fine.

Release 0.2

* Bugfix

Release 0.1

* Initial release